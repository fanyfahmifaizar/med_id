package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TokenRepo extends JpaRepository<Token, Long> {
    @Query("FROM Token WHERE Token = ?1")
    Optional<Token> FindActiveToken(String token);

    @Query("FROM Token WHERE UserId = ?1 AND IsExpired = false")
    Optional<Token> FindTokenByUserId(Long id);

    @Query("FROM Token WHERE Email = ?1 AND IsExpired = false")
    Optional<Token> FindTokenByEmail(String email);

    @Query("FROM Token WHERE UserId = ?1 ")
    List<Token> FindTokenByUserIdList(Long id);


}
