package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Biodata;
import com.example.med_id.med_id.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CustomerRepo extends JpaRepository<Customer,Long> {
    @Query("FROM Customer WHERE BiodataId = ?1 AND isDelete = false")
    Optional<Customer> getById(Long id);
}
