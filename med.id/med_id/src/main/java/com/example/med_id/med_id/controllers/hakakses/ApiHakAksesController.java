package com.example.med_id.med_id.controllers.hakakses;

import com.example.med_id.med_id.models.Role;
import com.example.med_id.med_id.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/hakakses")
public class ApiHakAksesController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/hakakses")
    public ResponseEntity<List<Role>> GetAllRole(){
            try {
                List<Role> roleData = this.roleRepo.getValueFalse();
                return new ResponseEntity<>(roleData, HttpStatus.OK);
            }
            catch(Exception exception){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
    }

    @PostMapping("/hakakses")
    public ResponseEntity<Object> SaveRole(@RequestBody Role role){
        try{
            role.setCreatedOn(new Date());
            role.setCreatedBy(1L);
            this.roleRepo.save(role);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/hakakses/{id}")
    public ResponseEntity<List<Role>> GetRoleById(@PathVariable("id") Long id){
        try{
            Optional<Role> role = this.roleRepo.findById(id);
            if(role.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/hakakses/{id}")
    public ResponseEntity<Object> UpdateRoleById(@RequestBody Role role, @PathVariable("id") Long id){
            Optional<Role> roleData = this.roleRepo.findById(id);
            if(roleData.isPresent()){
                role.setId(id);
                role.setModifiedOn(new Date());
                role.setModifiedBy(1L);
                this.roleRepo.save(role);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }

    @PutMapping("/hakaksesdelete/{id}")
    public ResponseEntity<Object> DeleteRoleById(@RequestBody Role role,@PathVariable("id") Long id)
    {
        Optional<Role> roleData = this.roleRepo.findById(id);
        if(roleData.isPresent()){
            role.setId(id);
            role.setDelete(true);
            role.setDeletedBy(1L);
            role.setDeletedOn(new Date());
            this.roleRepo.save(role);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/searchhakakses")
    public ResponseEntity<Map<String, Object>> GetRoleByName(@RequestParam String keyword,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){

        try {
            if (keyword != null) {

                List<Role> roleData = new ArrayList<>();

                Pageable paging = PageRequest.of(page, size, Sort.by("Name").ascending());

                Page<Role> role = this.roleRepo.searchRole(keyword, paging);

                roleData = role.getContent();

                Map<String, Object> response = new HashMap<>();
                response.put("role", roleData);
                response.put("currentPage", role.getNumber());
                response.put("totalItems", role.getTotalElements());
                response.put("totalPages", role.getTotalPages());

                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                List<Role> role = this.roleRepo.findAll();
                ResponseEntity la = new ResponseEntity<>(role, HttpStatus.OK);
                return la;
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/hakaksesmappedasc")
    public ResponseEntity<Map<String, Object>> GetRoleAllPageAsc(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){

        try{
            List<Role> roleData = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size, Sort.by("Name").ascending());


                Page<Role> pageTuts = roleRepo.findAllNotDeleted(paging);

                roleData = pageTuts.getContent();

                Map<String, Object> response = new HashMap<>();
                response.put("role" , roleData);
                response.put("currentPage", pageTuts.getNumber());
                response.put("totalItems", pageTuts.getTotalElements());
                response.put("totalPages", pageTuts.getTotalPages());

                return new ResponseEntity<>(response, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/hakaksesmappeddesc")
    public ResponseEntity<Map<String, Object>> GetRoleAllPageDesc(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){

        try{
            List<Role> roleData = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size, Sort.by("Name").descending());


            Page<Role> pageTuts = roleRepo.findAllNotDeleted(paging);

            roleData = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("role" , roleData);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/validasi/{keyword}")
    public ResponseEntity<List<Role>> ValidasiRoleByName(@PathVariable("keyword") String keyword){
        if (keyword != null){
            Optional<Role> role = this.roleRepo.validasi(keyword);
            ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
            return rest;
        }
        else{
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        }
    }
}
