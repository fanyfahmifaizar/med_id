package com.example.med_id.med_id.models;

import javax.persistence.*;

@Entity
@Table(name = "m_location")
public class Location extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name="name", length = 100, nullable = true)
    private String Name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    public Location locationParent;

    @Column(name = "parent_id", nullable = true)
    private Long ParentId;

    @OneToOne
    @JoinColumn(name = "location_level_id", insertable = false, updatable = false)
    public LocationLevel locationLevel;

    @Column(name = "location_level_id", nullable = true)
    private Long LocationLevelId;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Location getLocationParent() {
        return locationParent;
    }

    public void setLocationParent(Location locationParent) {
        this.locationParent = locationParent;
    }

    public Long getParentId() {
        return ParentId;
    }

    public void setParentId(Long parentId) {
        ParentId = parentId;
    }

    public LocationLevel getLocationLevel() {
        return locationLevel;
    }

    public void setLocationLevel(LocationLevel locationLevel) {
        this.locationLevel = locationLevel;
    }

    public Long getLocationLevelId() {
        return LocationLevelId;
    }

    public void setLocationLevelId(Long locationLevelId) {
        LocationLevelId = locationLevelId;
    }
}
