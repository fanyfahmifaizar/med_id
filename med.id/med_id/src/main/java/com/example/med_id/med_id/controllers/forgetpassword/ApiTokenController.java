package com.example.med_id.med_id.controllers.forgetpassword;


import com.example.med_id.med_id.models.Role;
import com.example.med_id.med_id.models.Token;
import com.example.med_id.med_id.models.User;
import com.example.med_id.med_id.repositories.TokenRepo;
import com.example.med_id.med_id.repositories.UserRepo;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;


import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/token")
public class ApiTokenController {
    @Autowired
    private TokenRepo tokenRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ApiForgetPasswordController apiForgetPasswordController;

    @Autowired
    private JavaMailSender javaMailSender;


    // Set token expired time
    private static final long OTPValidDuration = 10 * 60 * 1000;   // 10 minutes



    @GetMapping("/tokenverification/{token}")
    public ResponseEntity<Object> TokenVerification(@PathVariable("token") String token)
    {
        try
        {
            Optional<Token> tokenData = this.tokenRepo.FindActiveToken(token);
            List<Token> roleData = this.tokenRepo.findAll();

            if (tokenData.isPresent()) {

                // Get current time in milis
                Long currentDateTimeInMilis = System.currentTimeMillis();

                // Get value expiredOn
                Date expired = tokenData.get().getExpiredOn();

                // Get token expired in milis
                Long tokenExpiredtDateTimeInMilis = expired.getTime();

                // Get difference current & expired
                Long difference = tokenExpiredtDateTimeInMilis - currentDateTimeInMilis;

                // Convert difference to minutes
                Long differenceInMinutes = TimeUnit.MILLISECONDS.toMinutes(difference);

                if (differenceInMinutes >= 0 && differenceInMinutes <= 10 && tokenData.get().getExpired() == false) {

                    Map<String, Object> response = new HashMap<>();
                    response.put("msg", "Success");
                    response.put("userId", tokenData.get().getUserId());
                    response.put("tokenId", tokenData.get().getId());

                    return new ResponseEntity<>(response, HttpStatus.OK);
                }else {

                    Map<String, Object> response = new HashMap<>();
                    response.put("msg", "Error");
                    response.put("body", "Token expired");

                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }
            }

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Error");
            response.put("body", "Token not valid");

            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

        }

        catch (Exception e)
        {
            return ResponseEntity.notFound().build();
        }

    }

    @PutMapping("/changetokenstatus/{id}")
    public ResponseEntity<Map<String, Object>> ChangeTokenStatus(@RequestBody Token token, @PathVariable("id") Long id)
    {

        try {

            Optional<Token> tokenData = this.tokenRepo.FindTokenByUserId(id);

            // Lock token
            token.setId(tokenData.get().getId());
            token.setCreatedOn(tokenData.get().getCreatedOn());
            token.setCreatedBy(tokenData.get().getUserId());
            token.setModifiedOn(new Date());
            token.setModifiedBy(tokenData.get().getUserId());
            token.setExpiredOn(tokenData.get().getExpiredOn());
            token.setUserId(tokenData.get().getUserId());
            token.setEmail(tokenData.get().getEmail());
            token.setExpired(true);
            token.setToken(tokenData.get().getToken());
            token.setUsedFor(tokenData.get().getUsedFor());
            token.setDelete(true);

            this.tokenRepo.save(token);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            response.put("userId", tokenData.get().getUserId());

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/changefirsttokenstatus/")
    public ResponseEntity<Map<String, Object>> ChangeFirstTokenStatus(@RequestBody Token token)
    {
        try {

            Optional<Token> tokenData = this.tokenRepo.FindTokenByEmail(token.getEmail());

            // Lock token
            token.setId(tokenData.get().getId());
            token.setCreatedOn(tokenData.get().getCreatedOn());
            token.setCreatedBy(tokenData.get().getUserId());
            token.setModifiedOn(new Date());
            token.setModifiedBy(tokenData.get().getUserId());
            token.setExpiredOn(tokenData.get().getExpiredOn());
            token.setUserId(tokenData.get().getUserId());
            token.setEmail(tokenData.get().getEmail());
            token.setExpired(true);
            token.setToken(tokenData.get().getToken());
            token.setUsedFor(tokenData.get().getUsedFor());
            token.setDelete(true);

            this.tokenRepo.save(token);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            response.put("userId", tokenData.get().getUserId());

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/changetokenexpired/{id}")
    public ResponseEntity<Object> ChangeTokenExpired(@PathVariable("id") Long id, Token token)
    {
        try {
            Optional<Token> tokenData = this.tokenRepo.findById(id);

            // Lock token
            token.setId(id);
            token.setCreatedOn(tokenData.get().getCreatedOn());
            token.setCreatedBy(tokenData.get().getUserId());
            token.setModifiedOn(new Date());
            token.setModifiedBy(tokenData.get().getUserId());
            token.setExpiredOn(tokenData.get().getExpiredOn());
            token.setUserId(tokenData.get().getUserId());
            token.setEmail(tokenData.get().getEmail());
            token.setExpired(true);
            token.setToken(tokenData.get().getToken());
            token.setUsedFor(tokenData.get().getUsedFor());
            token.setDelete(true);

            this.tokenRepo.save(token);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            response.put("userId", tokenData.get().getUserId());

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Token sudah pernah digunakan, silahkan login ke akun anda", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/requestnewtoken")
    public ResponseEntity<Object> RequestNewToken(@RequestBody User user, Token token){
        Optional<User> userEmail = this.userRepo.findById(user.getId());
        if(userEmail.isPresent()){

            // Generate token
            String getToken = apiForgetPasswordController.GenerateToken();
            // Get timestamp
            Long currentDateTimeInMilis = System.currentTimeMillis();
            // Calculate token expired
            Long expiredOTOOn = currentDateTimeInMilis + OTPValidDuration;

            token.setEmail(userEmail.get().getEmail());
            token.setUsedFor("User verification");
            token.setUserId(user.getId());
            token.setToken(getToken);
            token.setCreatedBy(user.getId());
            token.setCreatedOn(new Date(currentDateTimeInMilis));
            token.setExpiredOn(new Date(expiredOTOOn));
            token.setExpired(false);

            this.tokenRepo.save(token);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            response.put("userId", user.getEmail());

            apiForgetPasswordController.sendMail(userEmail.get().getEmail(), getToken);

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        else{
            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Gagal");
            response.put("body", "Email sudah digunakan");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }


}
