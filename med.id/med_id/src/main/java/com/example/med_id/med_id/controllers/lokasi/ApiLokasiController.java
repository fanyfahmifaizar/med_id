package com.example.med_id.med_id.controllers.lokasi;

import com.example.med_id.med_id.models.Location;
import com.example.med_id.med_id.models.Role;
import com.example.med_id.med_id.repositories.LocationRepo;
import com.example.med_id.med_id.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/lokasi")
public class ApiLokasiController {

    @Autowired
    private LocationRepo locationRepo;

    @GetMapping("/lokasimapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){

        try{
            List<Location> locationData = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size,Sort.by("Id").ascending());


            Page<Location> pageTuts = locationRepo.findAllNotDeleted(paging);

            locationData = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("location" , locationData);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/lokasimappedfilter")
    public ResponseEntity<Map<String, Object>> getAllPagefilter(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size, @RequestParam Long id){

        if(id > 0) {
            List<Location> locationData = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size, Sort.by("Id").ascending());


            Page<Location> pageTuts = locationRepo.findAllNotDeletedFilter(id, paging);

            locationData = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("location", locationData);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else{
            List<Location> locationData = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size,Sort.by("Id").ascending());


            Page<Location> pageTuts = locationRepo.findAllNotDeleted(paging);

            locationData = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("location" , locationData);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    @GetMapping("/lokasi")
    public ResponseEntity<List<Location>> GetAllLokasi(){
        try {
            List<Location> locationData = this.locationRepo.getValueFalse();
            return new ResponseEntity<>(locationData, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/lokasibylevel/{id}")
    public ResponseEntity<List<Location>> GetLokasiByLevel(@PathVariable("id") Long id){
        try {
            List<Location> locationData = this.locationRepo.getByLevel(id);
            return new ResponseEntity<>(locationData, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/lokasi/{id}")
    public ResponseEntity<List<Location>> GetLokasiById(@PathVariable("id") Long id){
        try{
            Optional<Location> location = this.locationRepo.findById(id);
            if(location.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(location, HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/lokasi")
    public ResponseEntity<Object> PostLokasi(@RequestBody Location location){
        try{
            location.setCreatedOn(new Date());
            location.setCreatedBy(1L);
            this.locationRepo.save(location);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/lokasi/{id}")
    public ResponseEntity<Object> UpdateLocationId(@RequestBody Location location, @PathVariable("id") Long id){
        Optional<Location> locationData = this.locationRepo.findById(id);
        if(locationData.isPresent()){
            location.setId(id);
            location.setModifiedOn(new Date());
            location.setModifiedBy(1L);
            this.locationRepo.save(location);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/lokasibyparent/{id}")
    public ResponseEntity<List<Location>> GetLocationByParent(@PathVariable("id") Long id){
        try{
            List<Location> location = this.locationRepo.getByParent(id);
            return new ResponseEntity<>(location, HttpStatus.OK);
        }
        catch (Exception exception){
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/lokasidelete/{id}")
    public ResponseEntity<Object> DeleteLokasi(@RequestBody Location location,@PathVariable("id") Long id)
    {
        Optional<Location> locationData = this.locationRepo.findById(id);
        if(locationData.isPresent()){
            location.setId(id);
            location.setDelete(true);
            location.setDeletedBy(1L);
            location.setDeletedOn(new Date());
            this.locationRepo.save(location);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/getnullparentid")
    public ResponseEntity<List<Location>> GetAllNullParent(@RequestParam String keyword){
        try{
            if (keyword != null){
                Optional<Location> role = this.locationRepo.getNullParentId(keyword);
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            }
            else{
                return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
            }
        }
    catch (Exception e){

        return ResponseEntity.notFound().build();
    }
    }

    @GetMapping("/searchbynameandparent")
    public ResponseEntity<List<Location>> getByNameAndParent(@RequestParam String keyword,@RequestParam Long id){
        if (keyword != null){
            Optional<Location> role = this.locationRepo.getNotNullParentId(id,keyword);
            ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
            return rest;
        }
        else{
            return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/searchlokasi")
    public ResponseEntity<Map<String, Object>> SearchLokasi(@RequestParam String keyword,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size, @RequestParam(defaultValue="5") Long id){
       try{
           if (keyword != null) {

               if(id>0){
                   List<Location> locationData = new ArrayList<>();

                   Pageable paging = PageRequest.of(page, size, Sort.by("Name").ascending());

                   Page<Location> role = this.locationRepo.searchLocationAndLevel(keyword, id , paging);

                   locationData = role.getContent();

                   Map<String, Object> response = new HashMap<>();
                   response.put("location", locationData);
                   response.put("currentPage", role.getNumber());
                   response.put("totalItems", role.getTotalElements());
                   response.put("totalPages", role.getTotalPages());

                   return new ResponseEntity<>(response, HttpStatus.OK);
               }
               else {
                   List<Location> locationData = new ArrayList<>();

                   Pageable paging = PageRequest.of(page, size, Sort.by("Name").ascending());

                   Page<Location> role = this.locationRepo.searchLocation(keyword, paging);

                   locationData = role.getContent();

                   Map<String, Object> response = new HashMap<>();
                   response.put("location", locationData);
                   response.put("currentPage", role.getNumber());
                   response.put("totalItems", role.getTotalElements());
                   response.put("totalPages", role.getTotalPages());

                   return new ResponseEntity<>(response, HttpStatus.OK);
               }
           } else {
               List<Location> role = this.locationRepo.findAll();
               ResponseEntity la = new ResponseEntity<>(role, HttpStatus.OK);
               return la;
           }
       }
        catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
