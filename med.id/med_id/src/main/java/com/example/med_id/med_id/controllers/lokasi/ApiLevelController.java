package com.example.med_id.med_id.controllers.lokasi;

import com.example.med_id.med_id.models.Location;
import com.example.med_id.med_id.models.LocationLevel;
import com.example.med_id.med_id.repositories.LevelRepo;
import com.example.med_id.med_id.repositories.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/level")
public class ApiLevelController {

    @Autowired
    private LevelRepo levelRepo;

    @GetMapping("/level")
    public ResponseEntity<List<LocationLevel>> GetAllLevel(){
        try {
            List<LocationLevel> levelData = this.levelRepo.getValueFalse();
            return new ResponseEntity<>(levelData, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/level/{id}")
    public ResponseEntity<List<Location>> GetLevelById(@PathVariable("id") Long id){
        try{
            Optional<LocationLevel> level = this.levelRepo.findById(id);
            if(level.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(level, HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


}
