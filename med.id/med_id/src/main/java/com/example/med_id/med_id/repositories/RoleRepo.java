package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


public interface RoleRepo extends JpaRepository<Role, Long> {
    @Query("select f FROM Role f WHERE lower(Name) LIKE lower(concat('%',?1,'%')) And isDelete = false")
     Page<Role> searchRole(String keyword, Pageable pageable);

    @Query("FROM Role WHERE isDelete = false")
     List<Role> getValueFalse();

    @Query("SELECT f FROM Role f WHERE isDelete = false")
    Page<Role> findAllNotDeleted(Pageable pageable);

    @Query("FROM Role WHERE lower(Name) LIKE lower(concat(?1)) And isDelete = false ")
    Optional<Role> validasi(String keyword);
}
