package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Biodata;
import com.example.med_id.med_id.models.Location;
import com.example.med_id.med_id.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BiodataRepo extends JpaRepository<Biodata, Long> {
    @Query("FROM Biodata WHERE Id = ?1 AND isDelete = false")
    Optional<Biodata> getById(Long id);
}
