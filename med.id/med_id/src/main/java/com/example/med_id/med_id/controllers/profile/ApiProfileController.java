package com.example.med_id.med_id.controllers.profile;

import com.example.med_id.med_id.controllers.forgetpassword.ApiForgetPasswordController;
import com.example.med_id.med_id.models.*;
import com.example.med_id.med_id.repositories.BiodataRepo;
import com.example.med_id.med_id.repositories.CustomerRepo;
import com.example.med_id.med_id.repositories.TokenRepo;
import com.example.med_id.med_id.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/profile")
public class ApiProfileController {

    @Autowired
    private BiodataRepo biodataRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TokenRepo tokenRepo;

    @Autowired
    private ApiForgetPasswordController apiForgetPasswordController;

    private static final long OTPValidDuration = 10 * 60 * 1000;   // 10 minutes

    @GetMapping("/profile")
    public ResponseEntity<List<Biodata>> GetAllProfile(){
        try{
            Optional<Biodata> profile = this.biodataRepo.getById(3L);

            ResponseEntity rest = new ResponseEntity<>(profile, HttpStatus.OK);
            return rest;
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> GetAllCustomer(){
        try{
            Optional<Customer> lem = this.customerRepo.getById(3L);

            ResponseEntity rest = new ResponseEntity<>(lem, HttpStatus.OK);
            return rest;
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/profile/{id}")
    public ResponseEntity<Object> UpdateBiodata(@RequestBody Biodata biodata, @PathVariable("id") Long id){
        Optional<Biodata> locationData = this.biodataRepo.findById(id);
        if(locationData.isPresent()){
            biodata.setId(id);
            biodata.setModifiedOn(new Date());
            biodata.setModifiedBy(2L);
            biodata.setCreatedBy(locationData.get().getCreatedBy());
            biodata.setCreatedOn(locationData.get().getCreatedOn());
            this.biodataRepo.save(biodata);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<Object> UpdateCustomer(@RequestBody Customer customer, @PathVariable("id") Long id){
        Optional<Customer> locationData = this.customerRepo.getById(id);
        if(locationData.isPresent()){
            customer.setId(locationData.get().getId());
            customer.setModifiedOn(new Date());
            customer.setModifiedBy(2L);
            customer.setCreatedBy(locationData.get().getCreatedBy());
            customer.setCreatedOn(locationData.get().getCreatedOn());
            customer.setDob(customer.getDob());
            customer.setBiodataId(locationData.get().getBiodataId());
            customer.setGender(locationData.get().getGender());
            customer.setHeight(locationData.get().getHeight());
            customer.setRhesusType(locationData.get().getRhesusType());
            customer.setWeight(locationData.get().getWeight());
            this.customerRepo.save(customer);


            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/user")
    public ResponseEntity<List<User>> GetAllUser(){
        try{
            Optional<User> lem = this.userRepo.getById(3L);

            ResponseEntity rest = new ResponseEntity<>(lem, HttpStatus.OK);
            return rest;
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/userbybiodata/{id}")
    public ResponseEntity<List<User>> GetUserByBiodata(@PathVariable("id") Long id){
        try{
            Optional<User> role = this.userRepo.getById(id);
            if(role.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/tokeneditemail/{keyword}")
    public ResponseEntity<Object> PostTokenForEditEmail(@RequestBody User user, Token token, @PathVariable("keyword") String keyword){
        List<User> userEmail = this.userRepo.ListFindByEmail(keyword);
        Optional<User> currentEmail = this.userRepo.FindByEmail(user.getEmail());


        if (userEmail.isEmpty()) {
            if(currentEmail.isPresent()) {
                Long userId = currentEmail.get().getId();

                // Generate token
                String getToken = apiForgetPasswordController.GenerateToken();
                // Get timestamp
                Long currentDateTimeInMilis = System.currentTimeMillis();
                // Calculate token expired
                Long expiredOTOOn = currentDateTimeInMilis + OTPValidDuration;

                token.setEmail(user.getEmail());
                token.setUsedFor("User verification");
                token.setUserId(userId);
                token.setToken(getToken);
                token.setCreatedBy(userId);
                token.setCreatedOn(new Date(currentDateTimeInMilis));
                token.setExpiredOn(new Date(expiredOTOOn));
                token.setExpired(false);

                apiForgetPasswordController.sendMail(keyword, getToken);

                this.tokenRepo.save(token);

                Map<String, Object> response = new HashMap<>();
                response.put("msg", "Success");
                response.put("userId", userId);
                response.put("newEmail", keyword);


                return new ResponseEntity<>(response, HttpStatus.CREATED);
            }
            else{

                return ResponseEntity.notFound().build();
            }
        }
        else{

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Gagal");
            response.put("body", "Email sudah digunakan");

            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/editemail/{id}")
    public ResponseEntity<Object> EditEmailProfile(@RequestBody User user, Token token,@PathVariable("id") Long id){

        Optional<User> currentEmail = this.userRepo.findById(id);

        if(currentEmail.isPresent()) {
            Long userId = currentEmail.get().getId();

            user.setId(currentEmail.get().getId());
            user.setCreatedOn(currentEmail.get().getCreatedOn());
            user.setCreatedBy(currentEmail.get().getCreatedBy());
            user.setPassword(currentEmail.get().getPassword());
            user.setModifiedOn(new Date());
            user.setModifiedBy(currentEmail.get().getId());
            user.setEmail(user.getEmail());
            user.setBiodataId(currentEmail.get().getBiodataId());
            user.setDelete(currentEmail.get().getDelete());

            this.userRepo.save(user);
            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            response.put("userId", userId);


            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        else{

            return ResponseEntity.notFound().build();
        }


    }
}
