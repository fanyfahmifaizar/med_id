package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Location;
import com.example.med_id.med_id.models.LocationLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LevelRepo extends JpaRepository<LocationLevel,Long> {
    @Query("FROM LocationLevel WHERE isDelete = false")
    List<LocationLevel> getValueFalse();


}
