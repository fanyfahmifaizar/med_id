package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Location;
import com.example.med_id.med_id.models.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface LocationRepo extends JpaRepository<Location, Long> {
    @Query("SELECT f FROM Location f WHERE isDelete = false")
    Page<Location> findAllNotDeleted(Pageable pageable);

    @Query("SELECT f FROM Location f WHERE isDelete = false And LocationLevelId = ?1 ")
    Page<Location> findAllNotDeletedFilter(Long id,Pageable pageable);

    @Query("FROM Location WHERE isDelete = false")
    List<Location> getValueFalse();

    @Query("FROM Location WHERE isDelete = false And LocationLevelId = ?1 ")
    List<Location> getByLevel(long keyword);

    @Query("FROM Location WHERE isDelete = false And ParentId = ?1 ")
    List<Location> getByParent(long keyword);

    @Query("FROM Location WHERE isDelete = false And ParentId is null And lower(Name) LIKE lower(concat(?1))")
    Optional<Location> getNullParentId(String keyword);

    @Query("FROM Location WHERE isDelete = false And ParentId = ?1 And lower(Name) LIKE lower(concat(?2))")
    Optional<Location> getNotNullParentId(Long id, String keyword);

//    @Query("FROM Location WHERE lower(Name) LIKE lower(concat('%',?1,'%')) And isDelete = false ")
//    List<Location> searchLocation(String keyword);

    @Query("select a FROM Location a inner join LocationLevel b Where isDelete = false ")
    Page<Location> getAbsByLevelId(Pageable pegable);

    @Query("select f FROM Location f WHERE lower(Name) LIKE lower(concat('%',?1,'%')) And isDelete = false")
    Page<Location> searchLocation(String keyword, Pageable pageable);

    @Query("select f FROM Location f WHERE lower(Name) LIKE lower(concat('%',?1,'%')) And isDelete = false And LocationLevelId = ?2")
    Page<Location> searchLocationAndLevel(String keyword,Long level, Pageable pageable);


}
