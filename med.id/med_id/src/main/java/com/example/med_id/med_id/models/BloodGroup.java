package com.example.med_id.med_id.models;

import javax.persistence.*;

@Entity
@Table(name = "m_blood_group")
public class BloodGroup extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "code", length = 5, nullable = true)
    private String Code;

    @Column(name = "descrtiption", length = 255, nullable = true)
    private String Descrtiption;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getDescrtiption() {
        return Descrtiption;
    }

    public void setDescrtiption(String descrtiption) {
        Descrtiption = descrtiption;
    }
}
