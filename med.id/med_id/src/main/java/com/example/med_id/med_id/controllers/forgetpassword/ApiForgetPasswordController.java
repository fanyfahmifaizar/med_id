package com.example.med_id.med_id.controllers.forgetpassword;

import com.example.med_id.med_id.models.ResetPassword;
import com.example.med_id.med_id.models.Role;
import com.example.med_id.med_id.models.Token;
import com.example.med_id.med_id.models.User;
import com.example.med_id.med_id.repositories.ResetPasswordRepo;
import com.example.med_id.med_id.repositories.RoleRepo;
import com.example.med_id.med_id.repositories.TokenRepo;
import com.example.med_id.med_id.repositories.UserRepo;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.SecureRandom;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/lupapassword")
public class ApiForgetPasswordController {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TokenRepo tokenRepo;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private ResetPasswordRepo resetPasswordRepo;


    private static final long OTPValidDuration = 10 * 60 * 1000;   // 10 minutes

    public String GenerateToken()
    {
        Random rand = new Random(); //instance of random class
        int upperbound = 999999;
        //generate random values from 0-999999
        int int_random = rand.nextInt(upperbound);

        String token = String.valueOf(int_random);

        return token;
    }

    @PostMapping("/cekemail")
    public ResponseEntity<Object> CekEmailAndPostToken(@RequestBody User user, Token token){

        Optional<User> userEmail = this.userRepo.FindByEmail(user.getEmail());
        if(userEmail.isPresent()){

            Long userId = userEmail.get().getId();

            // Generate token
            String getToken = GenerateToken();
            // Get timestamp
            Long currentDateTimeInMilis = System.currentTimeMillis();
            // Calculate token expired
            Long expiredOTOOn = currentDateTimeInMilis + OTPValidDuration;

            token.setEmail(user.getEmail());
            token.setUsedFor("User verification");
            token.setUserId(userId);
            token.setToken(getToken);
            token.setCreatedBy(userId);
            token.setCreatedOn(new Date(currentDateTimeInMilis));
            token.setExpiredOn(new Date(expiredOTOOn));
            token.setExpired(false);

            this.tokenRepo.save(token);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            response.put("userId", userId);

            sendMail(user.getEmail(), getToken);

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        else{
            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Gagal");
            response.put("body", "Email sudah digunakan");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }


   public void sendMail(String userEmail, String token)
    {
        SimpleMailMessage msg = new SimpleMailMessage();

        msg.setTo(userEmail);
        msg.setFrom("support@med.id");
        msg.setSubject("Email verification");
        msg.setText("Hello, this is your Token: " + token + " Token will expired in 10 minutes.");

        javaMailSender.send(msg);
    }

    @PutMapping("/changepassword/{id}")
    public ResponseEntity<Map<String, Object>> ChangePasswordById(@RequestBody User user, @PathVariable("id") Long id)
    {
        try {
            Optional<User> userData = this.userRepo.findById(id);

            if (userData.isPresent())
            {
                // Encrypted password
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
                String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
                // Get timestamp
                Long currentDateTimeInMilis = System.currentTimeMillis();


                user.setId(id);
                user.setEmail(userData.get().getEmail());
                user.setLocked(userData.get().getLocked());
                user.setPassword(encodePassword);
                user.setCreatedOn(userData.get().getCreatedOn());
                user.setDelete(userData.get().getDelete());
                user.setCreatedBy(userData.get().getId());
                user.setBiodataId(userData.get().getBiodataId());

                this.userRepo.save(user);
                return new ResponseEntity<>(HttpStatus.OK);
            }

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/checkpassword")
    public ResponseEntity<Map<String, Object>> CheckPasswordById(@RequestBody User user){
        try{
            Optional<User> userData = this.userRepo.findById(1L);

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());

            //String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
            String encodePassword = user.getPassword();
            String dbPassword= userData.get().getPassword();

            if(bCryptPasswordEncoder.matches(encodePassword, dbPassword)){

                //String ponlo = userData.get().getEmail();
                return new ResponseEntity<>(HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        }
        catch (Exception e){
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping("/resetpassword/{id}")
    public ResponseEntity<Object> ResetPasswordById(@RequestBody User user, ResetPassword resetPassword,@PathVariable("id") Long id){
        Optional<User> userData = this.userRepo.findById(id);
        try{
            // Get timestamp
            Long currentDateTimeInMilis = System.currentTimeMillis();

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
            String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());


            resetPassword.setOldPassword(userData.get().getPassword());
            resetPassword.setNewPassword(encodePassword);
            resetPassword.setResetFor("Lupa Password");
            resetPassword.setCreatedBy(id);
            resetPassword.setCreatedOn(new Date(currentDateTimeInMilis));

            this.resetPasswordRepo.save(resetPassword);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        catch (Exception e){
            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Gagal");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/changenewpassword")
    public ResponseEntity<Map<String, Object>> ChangePassword (@RequestBody User user) {
//        try{
            Optional<User> userData = this.userRepo.findById(1L);

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());

            //String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
            String encodeNewPassword = user.getPassword();
            String dbPassword= userData.get().getPassword();

            if(bCryptPasswordEncoder.matches(encodeNewPassword, dbPassword)) {

                Map<String, Object> response = new HashMap<>();
                response.put("msg", "Gagal");
                response.put("body", "Password sudah digunakan");

                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
            else{
                if (userData.isPresent()) {

                    String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
                    // Get timestamp
                    Long currentDateTimeInMilis = System.currentTimeMillis();


                    user.setId(1L);
                    user.setEmail(userData.get().getEmail());
                    user.setLocked(userData.get().getLocked());
                    user.setPassword(encodePassword);
                    user.setCreatedOn(userData.get().getCreatedOn());
                    user.setDelete(userData.get().getDelete());

                    user.setCreatedBy(userData.get().getId());
                    user.setBiodataId(userData.get().getBiodataId());

                    this.userRepo.save(user);
                    return new ResponseEntity<>(HttpStatus.OK);
                }
                else{
                    return ResponseEntity.notFound().build();
                }
            }
//        }
//        catch (Exception e)
//        {
//            return ResponseEntity.notFound().build();
//        }
    }

    @PostMapping("/resetnewpassword")
    public ResponseEntity<Object> ResetPassword (@RequestBody User user, ResetPassword resetPassword){
        Optional<User> userData = this.userRepo.findById(1L);
        if(userData.isPresent()){
            // Get timestamp
            Long currentDateTimeInMilis = System.currentTimeMillis();

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
            String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());


            resetPassword.setOldPassword(userData.get().getPassword());
            resetPassword.setNewPassword(encodePassword);
            resetPassword.setResetFor("Ubah Password");
            resetPassword.setCreatedBy(1L);
            resetPassword.setCreatedOn(new Date(currentDateTimeInMilis));

            this.resetPasswordRepo.save(resetPassword);

            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Success");

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        else{
            Map<String, Object> response = new HashMap<>();
            response.put("msg", "Gagal");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
