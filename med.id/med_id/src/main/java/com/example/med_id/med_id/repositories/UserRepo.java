package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.Customer;
import com.example.med_id.med_id.models.Role;
import com.example.med_id.med_id.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
    @Query("FROM User WHERE Email = ?1 and isDelete = false")
    Optional<User> FindByEmail(String email);

    @Query("FROM User WHERE Email = ?1")
    List<User> ListFindByEmail(String email);

    @Query("FROM User WHERE lower(Email) LIKE lower(concat('%',?1,'%')) ")
    Optional<User> findUserByEmail(String keyword);

    @Query("FROM User WHERE BiodataId= ?1 AND isDelete = false")
    Optional<User> getById(Long id);

    @Query("FROM User WHERE Password= ?1 AND Id = ?2 AND isDelete = false")
    Optional<User> getByPassword(String password, Long id);

    @Query("FROM User u INNER JOIN Role r ON u.RoleId = r.Id WHERE u.Email =  ?1")
    User FindEmail(String email);

    @Query("FROM User WHERE Email = ?1")
    List<User> getPasswordByCurrentPassword(String password);


}
