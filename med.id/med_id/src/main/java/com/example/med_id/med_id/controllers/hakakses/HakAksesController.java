package com.example.med_id.med_id.controllers.hakakses;

import com.example.med_id.med_id.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value= "/hakakses/")
public class HakAksesController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping(value= "index")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("hakakses/index");
        return view;
    }
}
