package com.example.med_id.med_id.models;

import javax.persistence.*;

@Entity
@Table(name="t_reset_password")
public class ResetPassword extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name="old_password",length = 255, nullable = true)
    private String OldPassword;

    @Column(name="new_password",length = 255, nullable = true)
    private String NewPassword;

    @Column(name="reset_for", length = 20, nullable = true)
    private String ResetFor;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getOldPassword() {
        return OldPassword;
    }

    public void setOldPassword(String oldPassword) {
        OldPassword = oldPassword;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String newPassword) {
        NewPassword = newPassword;
    }

    public String getResetFor() {
        return ResetFor;
    }

    public void setResetFor(String resetFor) {
        ResetFor = resetFor;
    }
}
