package com.example.med_id.med_id.repositories;

import com.example.med_id.med_id.models.ResetPassword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResetPasswordRepo extends JpaRepository<ResetPassword,Long> {
}
